﻿# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 
#	Group 12
#
#	Team Members: Sebastian Cretes, Kevin Reyes, Richard Pham, Rik Mantel
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#	INFORMATION BELOW
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#	Please Note: We have created a new repository for the updated version of this project.
#	Please ensure you're accessing the correct information from within the correct repository. 

# !	Design Document: Code Repository (The Trip 2.0) on BitBucket
# ! Code Repository: Bit Bucket (https://bitbucket.org/cpscgroup12/the-trip-2.0/overview)

# ! Fourth Deliverable Details:
# 		Design Document for this deliverable is titled "DesignDocument.pdf" found in the new Code Repository.
# 		A new class for testing all classes is called MainTest.java. Please download this, include it in the same
#		file as the others, and run it in order to test the new classes. Facets of the previous version will be 
#                          included when they have a place in the grand scheme of things.

# ! How to Play the Current Game:
#		Download the needed files:
#			Game.java, EventHandler.java, Event.java, GameApplet.html, and all respective images.
#		Ensure they are within the same file with the images folder separate.
#		Compile Game.java. 
#		Type in 'appletviewer GameApplet.html' in order to run the applet of our game.	

# ! How to test the code:
#		Download the needed files:
#			MainTest.java, Game.java, EventHandler.java, Event.java.
#		Ensure they are within the same file and compile all.
#		
#		Refer to TesterInstrunctions.md