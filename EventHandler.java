/**
 *	EventHandler Class
 *	This class is basically the keeper of the Event code. Every event within the 
 *	the game is created here, ready to be passed onto the game when it's needed.
 */ 

public class EventHandler
{
	/**
	*	gameStart(): The introductory Event; it begins it all.
	*	
	*	@param: none.
	*	@return: event, the finished event object ready for use.
	*/
	public Event gameStart()
	{
		Event event = new Event("panel1.jpg", "Title", "body");
		return event;
	}
	
	/**
	*	firstEvent(): Exposition event to our story. 
	*	
	*	@param: none.
	*	@return: event, the finished event object ready for use.
	*/
	public Event firstEvent()
	{
		String imageURL = "images/panel2.jpg";
		String title = "The Beginning";
		String body = "It is a normal day, the sun is shining and you are ready to head out and meet with a friend. You meet up with your friend, who tells you about their week and a new friend they met on the previous weekend. You agree to head over to the new friend\'s house";
	
		Event event = new Event(imageURL, title, body);
		return event;
	}
	
	/**
	*	firstEvent(): The continuation of our story; introduction to the Candy Man.
	*	
	*	@param: none.
	*	@return: event, the finished event object ready for use.
	*/
	public Event candyMan()
	{
		String imageURL = "images/panel3.jpg";
		String title = "The Next";
		String body = "Your friend introduces you to the new guy, who happens to be a Magic Candy storeowner. He briefly describes his life and job as a public service for those with greater needs. He proceeds to offer to sell what he has in stock.";
		
		Event event = new Event(imageURL, title, body);
		return event;
	}
	
	/**
	*	dropCandy(): The explanation of the Drop Candy item. 
	*	
	*	@param: none.
	*	@return: event, the finished event object ready for use.
	*/
	public Event dropCandy()
	{
		String imageURL = "images/panel4.jpg";
		String title = "The Drop Candy";
		String body = "You decide to eat a <font color='red'><b>Drop Pop</b></font>, a candy that alters perception and cognition, open and closed eye visuals, hyper awareness of thoughts and emotions";
		
		Event event = new Event(imageURL, title, body);
		return event;
	}
	
	/**
	*	dropCandyA(): The result of eating the drop candy. 
	*	
	*	@param: none.
	*	@return: event, the finished event object ready for use.
	*/
	public Event dropCandyA()
	{
		String imageURL = "images/panel5.jpg";
		String title = "The Drop Candy";
		String body = "You and your friend decide to look at trippy things on the internet.";
		
		Event event = new Event(imageURL, title, body);
		return event;
	}
	
	/**
	*	dropCandyEnding(): The conclusion to the drop candy story. 
	*	
	*	@param: none.
	*	@return: event, the finished event object ready for use.
	*/
	public Event dropCandyEnding()
	{
		String imageURL = "images/panel6.jpg";
		String title = "The Drop Candy";
		String body = "After the spastic events that just occurred, you and your friend decided to call it a day at head home.";
		
		Event event = new Event(imageURL, title, body);
		return event;
	}
}