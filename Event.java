import javax.swing.ImageIcon;
/*
Event class that displays the title, portrait and narration in the game's panels
*/
public class Event
{
	private ImageIcon portraitIcon; 
	private String title;
	private String narration;
	
	// Constructor
	public Event(String imageURL, String eventTitle, String eventNarration)
	{
		portraitIcon = new ImageIcon(imageURL+"");
		title = eventTitle;
		narration = eventNarration; 
	}
	
	/*
	getIcon, getTitle and getNarration are all accessor methods
	getIcon returns the icon or image for the panel
	getTitle returns the panel's title
	getNarration returns the panel's dialog from the storyline in a specified location and size
	*/
	public ImageIcon getIcon()
	{
		return portraitIcon;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getNarration()
	{
		return "<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>" + narration + "</body></html>";
	}
}