/**
 *	Game Class
 *	The JApplet extension. The Game Class contains all of the goodies
 *	displayed on the Applet. Also handles what content to display.
 */

import javax.swing.*;
import java.awt.*;
import java.io.*;

import java.applet.Applet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.imageio.ImageIO;

public class Game extends JApplet implements ActionListener
{
	private Container contentPane;
	
	//	Instantiate our EventHandler to be used when deciding what content to display.
	//	Also set up a choices boolean. Potentially to display multiple options.
	private EventHandler events = new EventHandler();
	private boolean choices = false;

	//	Our Buttons for the player to interact with.
	private JButton nextButton;
	private JButton option2;
	private JButton option3;
	
	//	Some goodies for the management of the Events.
	private JLabel imagePort;
	private JLabel storyText;
	private int eventCount = 0;
	
	/**
	*	init(): dictates what's displayed on our Applet. 
	*	
	*	@param: none.
	*	@return: none.
	*/
	public void init()
	{	
		// Set up the Container and set its background.
		contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.setBackground(Color.BLACK);
		
		// Attempt to load our event image(s). 
		try
		{ 
			ImageIcon icon = new ImageIcon("images/titlescreen.gif"); 
			imagePort = new JLabel("Image and Text", icon, JLabel.CENTER); 
			
			// Add the image to the container.
			contentPane.add(imagePort, BorderLayout.PAGE_START);
		}
		catch(Exception e) {
			System.out.println("There was an error with loading the image.");
		 }
		
		// The title screen to our game. Sets the design for the game text.
		storyText = new JLabel("<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>Welcome to The Trip</body></html>");
		storyText.setFont(new Font("Corbel", Font.PLAIN, 14));
		storyText.setForeground(Color.white);
		
		// Add the text to the container.
		contentPane.add(storyText, BorderLayout.CENTER);
		
		// Setting up the continue button and giving it a custom look.
		nextButton = new JButton();
		nextButton.setBorderPainted(false);
 		nextButton.setBorder(null);
	 	nextButton.setFocusable(false);
		nextButton.setMargin(new Insets(0, 5, 0, 0));
	 	nextButton.setContentAreaFilled(false);
	 	nextButton.setIcon(new ImageIcon("images/next.jpg"));
	 	nextButton.setRolloverIcon(new ImageIcon("images/nextHover.jpg"));
	 	nextButton.setPressedIcon(new ImageIcon("images/next.jpg"));
	 	nextButton.setDisabledIcon(new ImageIcon("images/nextDisabled.jpg"));
		nextButton.addActionListener(this);
		
		// This will potentially be used for adding branches to the game.
		// As of right now, it is useless. We must further review this.
		if(choices == true)
		{
			JPanel subPanel = new JPanel();
			subPanel.setBackground(Color.BLACK);
			option2 = new JButton("Option2");
			option3 = new JButton("Option3");
			subPanel.add(option2);
			subPanel.add(option3);	
			contentPane.add(subPanel, BorderLayout.LINE_END);
		}else
		{
			//Add the continue button to our container.
			contentPane.add(nextButton, BorderLayout.LINE_END);	
		}
	}
	
	/**
	*	actionPerformed(ActionEvent e): dictates what code is executed when an item on the 
	*	applet is pressed. Good for all sorts of things.
	*	
	*	@param: none.
	*	@return: none.
	*/
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == nextButton)
		{
			eventCount++;
			if(eventCount == 1)
			{
				choices = true;
				Event e1 = events.firstEvent();
				imagePort.setIcon(e1.getIcon());
				storyText.setText(e1.getNarration());
				
			}else if(eventCount == 2)
			{
				Event e2 = events.candyMan();
				imagePort.setIcon(e2.getIcon());
				storyText.setText(e2.getNarration());
			}else if(eventCount == 3)
			{
				Event e3 = events.dropCandy();
				imagePort.setIcon(e3.getIcon());
				storyText.setText(e3.getNarration());
			}else if(eventCount == 4)
			{
				Event e4 = events.dropCandyA();
				imagePort.setIcon(e4.getIcon());
				storyText.setText(e4.getNarration());
			}else if(eventCount == 5)
			{
				Event e5 = events.dropCandyEnding();
				imagePort.setIcon(e5.getIcon());
				storyText.setText(e5.getNarration());
				nextButton.setEnabled(false);
			}
		}
	}
}