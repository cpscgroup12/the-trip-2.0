#	How to Test The Code: 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#	Compile all java files including: MainTest.java, Game.java, EventHandler.java, and Event.java.
# Run MainTest.java.
# Expected Output of Code:

The title for GameStart is: Title with  an Icon at: panel1.jpg and finaly the html for the narriation is: 
<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>body</body></html>.
The title for CandyMan is: The Next with  an Icon at: images/panel3.jpg and finaly the html for the narriation is: 
<html><body style='width: 500px; text-align: justify; padding-left: 25px;'>Your friend introduces you to the new guy, who happens to be a Magic Candy storeowner. He briefly describes his life and job as a public service for those with greater needs. He proceeds to offer to sell what he has in stock.</body></html>.