public class MainTest
{
    
    public static void eventsTest()
    {
	//Test the creation of events from eventHandler. 
	EventHandler e = new EventHandler();
	Event start = e.gameStart();
	System.out.println("The title for GameStart is: " + start.getTitle()+" with  an Icon at: "+ start.getIcon() + " and finaly the html for the narriation is: \n"+ start.getNarration() + "."); 

	Event candy = e.candyMan();
	System.out.println("The title for CandyMan is: " + candy.getTitle()+" with  an Icon at: "+ candy.getIcon() + " and finaly the html for the narriation is: \n"+ candy.getNarration() + "."); 
	
    }
    
    public static void main(String[] args)
    {
	eventsTest();
    }
}
