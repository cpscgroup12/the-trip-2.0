import java.io.*;
import java.util.*;
import java.lang.Math;
import java.awt.*;

//Actor class with information for the character/enemy

public class Actor {

	public String name;
	public Integer hp = new Integer(4);
	public Panel currentPanel;

//Constructor
	public Actor(String name, Room start) {
		this.name = name;
		this.hp = 100;
		this.currentPanel = start;
	}
	
//Player information
	public String information() {
		String out = "You are "+this.name;
		out += "\n" + this.hp();
		return out;
	}
	
/*//Changes the panel for the next event the player is in
	public String move(String direction) throws Exception
	{
		currentPanel = currentPanel.moveTo(direction);
		return currentPanel.information();
	}
	
//Gives the player their current panel location
	public String location()
	{
		return this.currentPanel.information();
	}
*/
	
//Damage inflicted on actor is removed from HP
    public void damageInflicted(int damage) {
    	hp = hp - damage;
    }
}

